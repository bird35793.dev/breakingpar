<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'cp919352_break' );

/** MySQL database username */
define( 'DB_USER', 'cp919352_break' );

/** MySQL database password */
define( 'DB_PASSWORD', 'P@ssw0rd2021!' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '0Yg`OgZswd)>kd$##!%@cSsEBFR*Ul7IgfL3=#JB@JQ0N<bbtzX)]b9UUX,j)mt0' );
define( 'SECURE_AUTH_KEY',  ']X/U_-YWrre?%RR5bG;-Z-Y|KGbncSY{u@}h*HfKMi]7B:EgWORqfqU$p3|eP.D7' );
define( 'LOGGED_IN_KEY',    '^I{L>FIBhQ&Ym@O)Z!*r:Cm)ABUw(aC;91`pt6M<q{DMP=GHF Q:3)9ItML-}%b&' );
define( 'NONCE_KEY',        '$5u2SFI5{@Z{#O,58EGk=r>ur^JuYUw#WT?7npO(;:0L%[d.oJ=gqUv(ch%_*Na0' );
define( 'AUTH_SALT',        '.BYxh_?yd,EsfpY%5~(,+9H.Vqfwr+kQ`K+4UhPK;LBq#;QX9aj,oNSUd.KEMVZk' );
define( 'SECURE_AUTH_SALT', 'l }o]iH,CMv~*02Zi)9+D4Tvk*zgF8x1n,}z/i2Ib3s| hLrjVA6FP|bcS=@R^lo' );
define( 'LOGGED_IN_SALT',   '`Q8)PpUnz~98o`R/r4^o:mc8?^.zVESI#M#;v`:f`T|ahWR@S=Zjwi%,<=g.8x}Q' );
define( 'NONCE_SALT',       '|8>1NK@-[a1@Snt^|~3.D6$?B]}rUO7bk`Z6dJ(kX~CevUPmx{@ C3P5[Thmn5ZG' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'bip_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
//Disable File Edits
define('DISALLOW_FILE_EDIT', true);